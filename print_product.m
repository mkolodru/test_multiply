% Usage: print_product.m num1 num2 fname
% Note: num1, num2, and fname should be defined elsewhere
fout=fopen(fname,'w');
fprintf(fout,'%f\n',num1*num2);
fclose(fout);
